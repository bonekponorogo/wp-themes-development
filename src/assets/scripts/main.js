/**
 * This script basically import jquery, popper.js and bootstrap
 * into one file. It is possible to import only required bootstrap
 * javascript modules to optimized the file size.
 */
import Popper from 'popper.js';
import '../scss/style.scss';

try {
	window.$ = window.jQuery = require('jquery');
	window.Popper = Popper;
	require('bootstrap');
} catch (e) {
	console.log(e);
}
