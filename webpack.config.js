/**
 * Webpack's configuration file
 *
 * Using main.js which import jquery, popper.js and bootstrap
 * altogether and fa.js which loads fontawesome svgs through
 * javascript. Additional libraries can be included to the template
 * by including them in the entry files of webpack.
 *
 * @since 1.0.0
 * @package wp-theme-dev
 */

const path = require('path');
const conf = require('./config.js');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
	entry: {
		...conf.scripts,
	},
	output: {
		path: conf.server.path,
		filename: 'scripts/[name].bundle.js'
	},
	plugins: [
		new CleanPlugin(conf.server.path),
		new CopyPlugin([
			{ from: 'src/templates', to: '' },
			{ from: 'src/statics', to: '' }
		]),
		new ExtractTextPlugin('style.css')
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [
					/node_modules/,
					/webroot/
				],
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: [
								require('@babel/preset-env')
							]
						}
					}
				]
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						'css-loader',
						{
							loader: 'postcss-loader',
							options: {
								plugins: function () {
									return [
										require('autoprefixer')
									]
								}
							}
						},
						'sass-loader'
					]
				})
			}
		]
	}
}
